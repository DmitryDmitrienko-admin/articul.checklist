<?
/* ALERTS */
$MESS["articul_checklist_TEST_SUCCESS"] = "Тест пройден успешно";
$MESS["articul_checklist_TEST_FAIL"] = "Тест не пройден";

/* CATEGORIES */
$MESS["articul_checklist_ARTICUL_GROUP_NAME_PROD"] = "Articul.Checklist: релиз";
$MESS["articul_checklist_ARTICUL_GROUP_NAME_DEV"] = "Articul.Checklist: разработка";
$MESS["articul_checklist_ARTICUL_GROUP_NAME_ARCHITECTURE"] = "Архитектура";
$MESS["articul_checklist_ARTICUL_GROUP_NAME_CACHE"] = "Кеширование";
$MESS["articul_checklist_ARTICUL_GROUP_NAME_HOSTING"] = "Хостинг";
$MESS["articul_checklist_ARTICUL_GROUP_NAME_WIKI"] = "Документация";
$MESS["articul_checklist_ARTICUL_GROUP_NAME_CONTENT"] = "Контент";
$MESS["articul_checklist_ARTICUL_GROUP_NAME_ARCHITECTURE"] = "Архитектура";
$MESS["articul_checklist_ARTICUL_GROUP_NAME_SECURITY"] = "Безопасность";
$MESS["articul_checklist_ARTICUL_GROUP_NAME_SEO"] = "SEO оптимизация";
$MESS["articul_checklist_ARTICUL_GROUP_NAME_COMMON"] = "Общие";
$MESS["articul_checklist_ARTICUL_GROUP_NAME_QUALITY"] = "Монитор качества";

/* TESTS */
$MESS["articul_checklist_COMMON_SITE_CHECK"] = "Проверка сайта [#SITE#]";
$MESS["articul_checklist_COMMON_URL_CHECK"] = "Проверка адреса #URL#";
$MESS["articul_checklist_COMMON_FILE_NOT_FOUND"] = "Файл <span style=\"color:red;\">[#FILE#]</span> не найден";
$MESS["articul_checklist_COMMON_FOLDER_NOT_FOUND"] = "Папка <span style=\"color:red;\">[#FOLDER#]</span> не найдена";
$MESS["articul_checklist_COMMON_FILE_FOUND"] = "Файл <span style=\"color:green;\">[#FILE#]</span> найден";
$MESS["articul_checklist_COMMON_FOLDER_FOUND"] = "Папка <span style=\"color:green;\">[#FOLDER#]</span> найдена";
$MESS["articul_checklist_COMMON_NO_FILES"] = "Указанные файлы недоступны: <span style=\"color:red;\">#FILES#</span>";
$MESS["articul_checklist_COMMON_PAGE_NOT_AVAILABLE"] = "<span style=\"color:red;\">Страница недоступна</span>";
$MESS["articul_checklist_COMMON_MODULE_NOT_FOUND"] = "Модуль <span style=\"color:red;\">#MODULE#</span> не установлен (<a href=\"/bitrix/admin/module_admin.php\" target=\"_blank\">все модули</a>)";

$MESS["articul_checklist_AM.ARCH.001_NAME"] = "На проекте настроен Git";
$MESS["articul_checklist_AM.ARCH.001_DESC"] = "На проекте должна использоваться система контроля версий (Git)";
$MESS["articul_checklist_AM.ARCH.001_HOWTO"] = "Проверяем наличие секции <b>remote</b> в файле настроек <b>.git/config</b>";
$MESS["articul_checklist_AM.ARCH.001_NOT_FOUND"] = "Файл <b>.git/config</b> не найден";

$MESS["articul_checklist_AM.ARCH.002_NAME"] = "Ядро CMS исключено из Git";
$MESS["articul_checklist_AM.ARCH.002_DESC"] = "Из Git должны быть исключены папки <b>bitrix</b> и <b>upload</b>, а так же файл <b>.htaccess</b>";
$MESS["articul_checklist_AM.ARCH.002_HOWTO"] = "Проверяем файл .gitignore на наличие строк <b>bitrix</b>, <b>upload</b> и  <b>.htaccess</b>";
$MESS["articul_checklist_AM.ARCH.002_NOT_FOUND"] = "Файл <b>.gitignore</b> не найден";

$MESS["articul_checklist_AM.ARCH.003_NAME"] = "Для каждой языковой версии создан отдельный сайт";

$MESS["articul_checklist_AM.ARCH.004_NAME"] = "Для каждой страницы в одном дизайне используется один шаблон сайта";

$MESS["articul_checklist_AM.ARCH.005_NAME"] = "Архитектура и CodeStyle соответствуют регламенту";
$MESS["articul_checklist_AM.ARCH.005_DESC"] = "Проект должен быть разработан в рамках регламента, принятого в компании, и фреймворка 1С-Битрикс, а так же соблюдать <a href=\"https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=43&CHAPTER_ID=03044&LESSON_PATH=3913.4776.4774.3044\" target=\"_blank\">Code Style Guide</a> 1С-Битрикс.";

$MESS["articul_checklist_AM.ARCH.006_NAME"] = "Все кастомные компоненты, шаблоны и служебные скрипты расположены в /local/";
$MESS["articul_checklist_AM.ARCH.006_HOWTO"] = "Проверяется наличие папки <b>/local/</b> в корне каждого сайта.";

$MESS["articul_checklist_AM.CACHE.001_NAME"] = "Включено автокеширование";
$MESS["articul_checklist_AM.CACHE.001_DESC"] = "Должно быть включено автокеширование в <a href=\"/bitrix/admin/cache.php\" target=\"_blank\">настройках</a> главного модуля (повторяет тест QP0030).";

$MESS["articul_checklist_AM.CACHE.002_NAME"] = "Компоненты с типовыми запросами кешируются";
$MESS["articul_checklist_AM.CACHE.003_NAME"] = "Кеш не зависит от ID пользователя и других уникальных параметров";

$MESS["articul_checklist_AM.CONT.001_NAME"] = "Заполнен robots.txt";
$MESS["articul_checklist_AM.CONT.001_DESC"] = "В корне сайта должен быть файл <b>robots.txt</b> с инструкциями для поисковых роботов.";
$MESS["articul_checklist_AM.CONT.001_HOWTO"] = "Проверяется наличие файла <b>robots.txt</b> в корне сайта.";

$MESS["articul_checklist_AM.CONT.002_NAME"] = "Присутствует favicon.ico";
$MESS["articul_checklist_AM.CONT.002_DESC"] = "В корне сайта должен быть файл <b>favicon.ico</b>.";
$MESS["articul_checklist_AM.CONT.002_HOWTO"] = "Проверяется наличие файла <b>favicon.ico</b> в корне сайта.";

$MESS["articul_checklist_AM.CONT.003_NAME"] = "Указаны apple-touch-icon";
$MESS["articul_checklist_AM.CONT.003_DESC"] = "Должны быть указаны <b>apple-touch-icon</b> для мобильных устройств.";
$MESS["articul_checklist_AM.CONT.003_HOWTO"] = "В корне сайта должен быть файл <b>apple-touch-icon.png</b>.<br/>Если его нет, проверяется наличие тега <b>link</b> c атрибутом <b>rel</b>, указывающего на <b>apple-touch-icon</b> по умолчанию. Обратите внимание, что вы можете указать несколько файлов разных размеров для поддержки различных моделей устройств (<a href=\"https://developer.apple.com/library/mac/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html\" target=\"_blank\">подробнее</a>).";
$MESS["articul_checklist_AM.CONT.003_NO_TAG"] = "Не указаны теги <span style=\"color:red;\">&lt;link rel=\"apple-touch-icon\" href=\"...\"&gt;</span>";
$MESS["articul_checklist_AM.CONT.003_MORE_INFO"] = "Подробнее об apple-touch-icon: <a href=\"https://developer.apple.com/library/mac/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html\" target=\"_blank\">developer.apple.com</a>.";

$MESS["articul_checklist_AM.CONT.004_NAME"] = "У тега html должен быть указан атрибут lang";
$MESS["articul_checklist_AM.CONT.004_TAG_NOT_FOUND"] = "<span style=\"color:red;\">lang</span> не указан или пустой";
$MESS["articul_checklist_AM.CONT.004_TAG_FOUND"] = "<span style=\"color:green;\">lang</span> указан";
$MESS["articul_checklist_AM.CONT.004_MORE_INFO"] = "Подробнее об атрибуте lang: <a href=\"http://htmlbook.ru/html/attr/lang\" target=\"_blank\">htmlbook.ru</a>.";

$MESS["articul_checklist_AM.CONT.005_NAME"] = "Указаны Open Graph теги";
$MESS["articul_checklist_AM.CONT.005_DESC"] = "На всех страницах должны быть Open Graph теги для социальных сетей:
                                                <br/>&mdash; og:title
                                                <br/>&mdash; og:description
                                                <br/>&mdash; og:type
                                                <br/>&mdash; og:url
                                                <br/>&mdash; og:image";
$MESS["articul_checklist_AM.CONT.005_TAG_NOT_FOUND"] = "<span style=\"color:red;\">[#TAG#]</span> не указан или пустой";
$MESS["articul_checklist_AM.CONT.005_TAG_FOUND"] = "<span style=\"color:green;\">[#TAG#]</span> указан";
$MESS["articul_checklist_AM.CONT.005_MORE_INFO"] = "Подробнее об Open Graph тегах: <a href=\"http://ogp.me/\" target=\"_blank\">ogp.me</a>.";

$MESS["articul_checklist_AM.CONT.006_NAME"] = "Настроен ЧПУ для всего сайта";

$MESS["articul_checklist_AM.CONT.007_NAME"] = "Есть нормальная 404 страница";

$MESS["articul_checklist_AM.SECR.001_NAME"] = "Конфиг Git не доступен по HTTP";
$MESS["articul_checklist_AM.SECR.001_DESC"] = "Файл <b>/.git/config</b> не должен быть доступен по HTTP (http://domain/.git/config).";
$MESS["articul_checklist_AM.SECR.001_FAIL"] = "Адрес <span style=\"color:red;\">#URL#</span> доступен по HTTP.
                                                <br>В папке <b>/.git/</b> создайте файл <b>.htaccess</b> с текстом <b>deny from all</b>, чтобы закрыть доступ к конфигу.";
$MESS["articul_checklist_AM.SECR.002_NAME"] = "Админка недоступна любому авторизованному пользователю";
$MESS["articul_checklist_AM.SECR.002_HOWTO"] = "Авторизуйтесь под любым пользователем, не входящим в группу <b>Администраторы</b> и убедитесь, что у него нет доступа в административную панель.";

$MESS["articul_checklist_AM.SECR.003_NAME"] = "В репозитории нет и не было коннектов к базе и других приватных данных";
$MESS["articul_checklist_AM.SECR.003_HOWTO"] = "Убедитесь, что в репозитории нет файлов:
                                                <ul>
                                                    <li>/bitrix/php_interface/dbconn.php</li>
                                                    <li>/bitrix/.settings.php</li>
                                                    <li>и других файлов, содержащих ключи и пароли</li>
                                                </ul>";

$MESS["articul_checklist_AM.SECR.004_NAME"] = "На публичных страницах и в браузерной консоли нет отладочной информации";

$MESS["articul_checklist_AM.SECR.005_NAME"] = "Репозиторий на Bitbucket или GitHub приватный";

$MESS["articul_checklist_AM.WIKI.001_NAME"] = "Все константы описаны в Wiki репозитория";

$MESS["articul_checklist_AM.WIKI.002_NAME"] = "Все интегрированные внешние сервисы описаны в Wiki репозитория";

$MESS["articul_checklist_AM.WIKI.003_NAME"] = "Все агенты и задачи на Cron описаны в Wiki репозитория";

$MESS["articul_checklist_AM.COMN.001_NAME"] = "Настроено автообновление dev/demo/preprod площадок";
$MESS["articul_checklist_AM.COMN.001_HOWTO"] = "Если у вас должны быть автообновляемые dev/demo/preprod площадки, проверьте, что вы не забыли их настроить.";

$MESS["articul_checklist_AM.SEO.001_NAME"] = "Учтены рекомендации по SEO";
$MESS["articul_checklist_AM.SEO.001_HOWTO"] = "Должны быть учтены рекомендации по SEO (<a href=\"http://st.cmsmagazine.ru/images/articles/seo-cheat-sheet/perevod_SEOCheatSheet_2-2013_new.pdf
\" target=\"_blank\">открыть</a>).";

$MESS["articul_checklist_AM.SEO.002_NAME"] = "Для мобильной версии указан канонический url";
$MESS["articul_checklist_AM.SEO.002_DESC"] = "Если мобильная версия расположена на отдельном адресе (не адаптивный сайт), необходимо разметить:
                                                <ul>
                                                    <li>Страницы настольной версии
                                                        <br/>
                                                        &lt;link rel=\"alternate\" media=\"only screen and (max-width: 640px)\" href=\"http://m.example.com/\" /&gt;
                                                    </li>
                                                    <li>Страницы мобильной версии
                                                        <br/>
                                                        &lt;link rel=\"canonical\" href=\"http://example.com/\" /&gt;
                                                    </li>
                                                </ul>
                                                <br/>
                                                Подробнее о канонических страницах: <a href=\"https://support.google.com/webmasters/answer/139066?hl=ru\" target=\"_blank\">support.google.com</a>.";

$MESS["articul_checklist_AM.COMN.101_NAME"] = "Активирован ключ Битрикс";
$MESS["articul_checklist_AM.COMN.101_HOWTO"] = "Откройте <a href=\"/bitrix/admin/update_system.php?refresh=Y\" target=\"_blank\">Систему обновлений</a> и убедитесь, что активирован не DEMO ключ, и обновления устанавливаются.";

$MESS["articul_checklist_AM.COMN.102_NAME"] = "Подключен Google Webmaster Tools";
$MESS["articul_checklist_AM.COMN.102_DESC"] = "Сайт должен быть подключен к <a href=\"https://www.google.com/webmasters/tools/home?hl=ru\" target=\"_blank\">Google Webmaster Tools</a> &mdash; инструменту, который сообщает вам о подозрительных активностях, например, о внедрении вредоносного кода, и других проблемах на сайте.";

$MESS["articul_checklist_AM.COMN.103_NAME"] = "Настроен рабочий email &laquo;по умолчанию&raquo;";
$MESS["articul_checklist_AM.COMN.103_HOWTO"] = "Убедитесь, что в настройках <a href=\"/bitrix/admin/settings.php?lang=ru&mid=main&mid_menu=1\" target=\"_blank\">Главного модуля</a> и <a href=\"/bitrix/admin/site_admin.php\" target=\"_blank\">всех сайтов</a> указан рабочий email &laquo;по умолчанию&raquo;.";

$MESS["articul_checklist_AM.HOST.101_NAME"] = "На сервере установлен Git";
$MESS["articul_checklist_AM.HOST.101_HOWTO"] = "Проверяется результат выполнения команды <b>git help</b>.";
$MESS["articul_checklist_AM.HOST.101_GIT_NOT_FOUND"] = "Возможно, Git не установлен.
                                                        <br/>Результат выполнения <b>git help</b>:
                                                        <br/>#RESPONSE#";

$MESS["articul_checklist_AM.HOST.102_NAME"] = "Есть доступ на запись по SSH или FTP";

$MESS["articul_checklist_AM.HOST.103_NAME"] = "Проверка системы не выявила критических ошибок";
$MESS["articul_checklist_AM.HOST.103_HOWTO"] = "Выполните проверку системы <a href=\"/bitrix/admin/site_checker.php\" target=\"_blank\">встроенными средствами</a>, в отчёте не должно быть критических ошибок.
                                                <br/>При необходимости проверьте сервер скриптом <a href=\"http://dev.1c-bitrix.ru/download/scripts/bitrix_server_test.php\" target=\"_blank\">bitrix_server_test.php</a>.";

$MESS["articul_checklist_AM.HOST.104_NAME"] = "Используется один из вариантов домена: с www или без www";
$MESS["articul_checklist_AM.HOST.104_DESC"] = "У каждой страницы должен быть уникальный адрес, поэтому адреса вида <b>http://www.domain.com</b> должны перенаправляться на <b>http://domain.com/</b> или наоборот.";
$MESS["articul_checklist_AM.HOST.104_HOWTO"] = "Если ваш сайт открывается и по домену с www (www.domain.com) и без www (domain.com), вы можете настроить редиректы, добавив в <b>.htaccess</b> запись:
                                                <pre>
                                                    <br/># redirect www
                                                    <br/>RewriteCond %{HTTP_HOST} ^www.domain\.com
                                                    <br/>RewriteRule ^(.*)$ http://domain.com/$1 [R=301,L]
                                                </pre>";

$MESS["articul_checklist_AM.HOST.105_NAME"] = "Адреса страниц заканчиваются слешем";
$MESS["articul_checklist_AM.HOST.105_DESC"] = "У каждой страницы должен быть уникальный адрес, поэтому адреса вида <b>http://domain.com</b> должны перенаправляться на <b>http://domain.com/</b>.";
$MESS["articul_checklist_AM.HOST.105_HOWTO"] = "Откройте любую страницу вашего сайта, кроме главной: если настроены ЧПУ, адрес должен заканчиваться слешем.
                                                <br/>Если это не так, вы можете настроить редиректы, добавив в <b>.htaccess</b> запись:
                                                <pre>
                                                    <br/># add trailing slash
                                                    </br>RewriteCond %{REQUEST_FILENAME} !-f
                                                    <br/>RewriteRule ^.*[^/]$ /$0/ [L,R=301]
                                                </pre>";

$MESS["articul_checklist_AM.COMN.104_NAME"] = "Настроено периодическое автоматическое резервное копирование";
$MESS["articul_checklist_AM.COMN.104_DESC"] = "Не забудьте сохранить пароль от бекапа в безопасном месте (например, в KeePass или Рабочей группе по проекту).";
$MESS["articul_checklist_AM.COMN.104_AUTO_BACKUP_DISABLED"] = "Автоматическое резервное копирование не настроено (<a href=\"/bitrix/admin/bitrixcloud_backup_job.php\" target=\"_blank\">расписание</a>).
                                                                <br/><a href=\"/bitrix/admin/dump_auto.php\" target=\"_blank\">Включите</a> регулярное резервное копирование.";

$MESS["articul_checklist_AM.COMN.105_NAME"] = "Настроен инспектор сайтов";
$MESS["articul_checklist_AM.COMN.105_DESC"] = "Не забудьте указать рабочий email для рассылки уведомлений.
                                                <br/><a href=\"https://dev.1c-bitrix.ru/community/blogs/product_features/cloud_inspector.php\" target=\"_blank\">Подробнее</a> об инспекторе сайтов.";
$MESS["articul_checklist_AM.COMN.105_MORE_INFO"] = "<a href=\"/bitrix/admin/bitrixcloud_monitoring_admin.php\" target=\"_blank\">Настроить</a> инспектор сайтов.";
$MESS["articul_checklist_AM.COMN.105_MONITORING_DISABLED"] = "Мониторинг <span style=\"color: red\">выключен</span>";
$MESS["articul_checklist_AM.COMN.105_MONITORING_ENABLED"] = "Мониторинг <span style=\"color: green\">включен</span>";

$MESS["articul_checklist_AM.SECR.101_NAME"] = "Сканер безопасности не выявил критичных ошибок";
$MESS["articul_checklist_AM.SECR.101_HOWTO"] = "Выполните проверку системы <a href=\"/bitrix/admin/security_scanner.php\" target=\"_blank\">встроенными средствами</a>, в отчёте не должно быть критических ошибок.";

$MESS["articul_checklist_AM.QMON.101_NAME"] = "Проект сдан по Монитору качества, заявка на анкетирование заполнена и отправлена";