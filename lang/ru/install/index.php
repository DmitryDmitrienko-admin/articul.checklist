<?
$MESS["articul_checklist_MODULE_NAME"] = "Articul Checklist";
$MESS["articul_checklist_MODULE_DESC"] = "Автоматические тесты Articul Media для монитора качества";
$MESS["articul_checklist_PARTNER_NAME"] = "Articul Production";
$MESS["articul_checklist_PARTNER_URI"] = "http://www.articulmedia.ru/";
$MESS["articul_checklist_NO_ACCESS"] = "У вас нет прав на это действие";
$MESS["articul_checklist_SUCCESS_PAGE_TITLE"] = "Тесты качества Articul.Checklist";
?>