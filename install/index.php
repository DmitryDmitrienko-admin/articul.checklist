<?
IncludeModuleLangFile(__FILE__);
class articul_checklist extends CModule
{
	var $MODULE_ID = "articul.checklist";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;

	function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__)."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("articul_checklist_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("articul_checklist_MODULE_DESC");
		$this->PARTNER_NAME = GetMessage("articul_checklist_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("articul_checklist_PARTNER_URI");
	}

	function InstallFiles()
	{
		RegisterModuleDependences("main", "onCheckListGet", $this->MODULE_ID, "Articul\Checklist\Test", "onCheckListGet");
		return true;
	}

	function UnInstallFiles()
	{
		UnRegisterModuleDependences("main", "onCheckListGet", $this->MODULE_ID, "Articul\Checklist\Test", "onCheckListGet");
		return true;
	}	
	function DoInstall()
	{
		global $APPLICATION;
		if ($GLOBALS['APPLICATION']->GetGroupRight('main') < 'W') 
		{
			CAdminMessage::ShowMessage(GetMessage("articul_checklist_NO_ACCESS"));
			return false;
		}
		$this->InstallFiles();
		RegisterModule($this->MODULE_ID);

        $APPLICATION->IncludeAdminFile(
            GetMessage("articul_checklist_SUCCESS_PAGE_TITLE"),
            dirname(__FILE__)."/success.php"
        );
	}

	function DoUninstall()
	{
		global $APPLICATION;
		if ($GLOBALS['APPLICATION']->GetGroupRight('main') < 'W')
		{
			CAdminMessage::ShowMessage(GetMessage("articul_checklist_NO_ACCESS"));
			return false;
		}
		$this->UnInstallFiles();
		UnRegisterModule($this->MODULE_ID);
	}
}
?>
