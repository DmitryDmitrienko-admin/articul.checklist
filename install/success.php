<? IncludeModuleLangFile(__FILE__); ?>
<?=CAdminMessage::ShowMessage(array(
    "MESSAGE" => GetMessage("articul_checklist_SUCCESS_TEXT"),
    "TYPE" => "OK"
));?>
<p>
    <a class="adm-btn adm-btn-green adm-btn-add" href="/bitrix/admin/checklist.php"><?=GetMessage("articul_checklist_SUCCESS_BUTTON")?></a>
</p>